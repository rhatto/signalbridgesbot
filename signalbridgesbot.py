#!/usr/bin/env python
#
# Signalbridgesbot: A simple Tor bridge, website mirror, and
# Tor Browser distributor bot for Signal Private Messenger. 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Signal Tor Bridges Bot
    - Set message expiration to 1 day
    - Share a Tor bridge automatically
    - Pull bridges from a txt file
"""
import os
#from pathlib import Path

import random

from semaphore import Bot, ChatContext


async def expiration(ctx: ChatContext) -> None:
    if not ctx.message.empty():
        await ctx.message.mark_read()

        receiver = ctx.message.get_group_id()
        if receiver is None:
            receiver = ctx.message.source.uuid

        await ctx.bot.set_expiration(receiver, 86400)


async def onion(ctx: ChatContext) -> None:
    await ctx.message.reply(body="🧅", reaction=True)


async def bridgereply(ctx: ChatContext) -> None:
    if not ctx.message.empty():
       await ctx.message.typing_started()
       await ctx.message.reply("Hello! I'm a bot. 🤖  Here is a Tor bridge line:")
       # Pull bridges from files/bridgeslist.txt 
       random_lines = random.choice(open("files/bridgeslist.txt").readlines())
       await ctx.message.reply(random_lines)
       await ctx.message.reply("Please remember to copy the complete bridge address and paste them as it is.")
       await ctx.message.reply("You can access the Tor Browser user documentation here: https://tb-manual.torproject.org "
                               "or if you need help, please send a message https://t.me/@TorProjectSupportBot. Bye!")
       await ctx.message.typing_stopped()


async def main() -> None:
    """Start the bot."""
    # Connect the bot to number.
    async with Bot(os.environ["SIGNAL_PHONE_NUMBER"]) as bot:
        bot.register_handler("", expiration)

        # React to message.
        bot.register_handler("", onion)

        # Set profile name.
        await bot.set_profile("Tor Bridges Bot")

        # Send the bridge line.
        bot.register_handler("", bridgereply)

        # Run the bot until you press Ctrl-C.
        await bot.start()


if __name__ == '__main__':
    import anyio
    anyio.run(main)
