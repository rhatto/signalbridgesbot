# Signal bridges bot

A simple bridges bot using [Semaphore](https://github.com/lwesterhof/semaphore/) library.

## Requirements

* [signald](https://gitlab.com/signald/signald) 0.17.0 or later, a daemon that facilitates communication over Signal.
* [Semaphore](https://github.com/lwesterhof/semaphore/).

## Installation

1. Install `signald` and register a phone number. Follow these instructions: https://github.com/lwesterhof/semaphore/#quick-start
2. Install Semaphore:

`pip install semaphore-bot`

3. Run signalbridgesbot

`python3 signalbridgesbot.py  `

## Project status

**Important:** Signal data (contact username, avatar, key, about) is saved on `signald` config file: `.config/signald/data/+phonenumber`.

At the moment, Signal Bridges Bot will

- Set message expiration to 1 day
- Share a Tor bridge automatically
- Shuffle and handle different bridges for the same user
- Read bridges from a file

#### TODO

- Send the same bridge for the same user
- GetTor: Add GetTor Google Drive link
- Export bot metrics
- Localize the text based on user country code (priority: RU, ZH, FA)
- Build mitigations against enumeration:
    - Build a suspicious list: if a user request bridges more than 5x per day, add their UUID to a blocklist.txt
